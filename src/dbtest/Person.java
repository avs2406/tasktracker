/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbtest;

/**
 *
 * @author Admin
 */
public class Person {

private int id;    
private String name="Новое имя";
private String lastName="";
private String email="";
private String post="";

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPost() {
        return post;
    }

    @Override
    public String toString() {
      if (this.lastName.length()>0)
          return this.lastName+" "+this.name;
      else{
          return this.name;
      }
    }


    
}
