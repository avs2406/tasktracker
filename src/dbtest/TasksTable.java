/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbtest;

import java.awt.PageAttributes;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class TasksTable {

    private String tableName = "task";

    //фабричный метод
    private Task produceTaskByRs(ResultSet rs) throws SQLException {
        Task resultTask = new Task();
        resultTask.setId(rs.getInt("id"));
        resultTask.setName(rs.getString("name"));
        resultTask.setDescription(rs.getString("description"));
        resultTask.setComment(rs.getString("comment"));
        resultTask.setPerson_id(rs.getInt("person_id"));
        resultTask.setStatus(rs.getString("status"));
        return resultTask;
    }

    Task getById(int id) {
        try {
            Statement stmt = DBManager.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("Select * from " + this.tableName + " where id= " + id + ";");
            if (rs.next()) {
                return this.produceTaskByRs(rs);
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TasksTable.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * GetList на стероидах
     *
     * @param orderBy - поле сортировки
     * @param orderDir - порядок сортировки
     * @param persons - массив Id исполнителей (если для всех, то передать
     * пустой массив)
     * @param limit - ограничение количества
     * @param page - номер страницы
     * @return ArrayList<Task> - массив объектов задач
     */
    public ArrayList<Task> getList(String orderBy, String orderDir, ArrayList<Integer> persons, int limit, int page) {
        try {
            Statement stmt = DBManager.getConnection().createStatement();

            String where = "";

            if (persons.size() > 0) {
                where = "Where task.person_id IN (";

                for (int id : persons) {
                    where += id + ",";
                }
                where = where.substring(0, where.length() - 1);
                where += ") ";
            }            

            ResultSet rs = stmt.executeQuery("SELECT * FROM "
                    + this.tableName + " \n"
                    + where
                    + "ORDER BY task." + orderBy + " " + orderDir
                    + " LIMIT " + limit + " OFFSET " + limit * (page - 1) + " \n"
                    + ";");

            ArrayList<Task> tasks = new ArrayList<Task>();

            while (rs.next()) {
                tasks.add(this.produceTaskByRs(rs));
            }

            return tasks;

        } catch (SQLException ex) {
            Logger.getLogger(TasksTable.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * GetList с параметрами по-умолчанию
     *
     * @return
     */
    public ArrayList<Task> getList() {
        return this.getList("id", "ASC", new ArrayList<Integer>(), 1000, 1);
    }
    
    /**
     * GetList c фильтром по ID (список задач по исполнителю)
     * @param id
     * @return 
     */
public ArrayList<Task> getList(int personId){
    ArrayList<Integer> filter =new ArrayList<Integer>();
    filter.add(personId);
    return getList("id","DESC",filter,1000,1);
}
    
    boolean deleteById(int id) {
        try {
            Statement stmt = DBManager.getConnection().createStatement();

            stmt.execute("DELETE FROM " + this.tableName + " WHERE id=" + id + ";");

        } catch (SQLException ex) {
            Logger.getLogger(TasksTable.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
    
   

    public int insertTask(Task task) {

        try {
            Statement stmt = DBManager.getConnection().createStatement();

            int resultId = stmt.executeUpdate("INSERT INTO " + tableName + " (`name`, `description`, `comment`, `status`)VALUES ("
                    + "'" + task.getName() + "', "
                    + "'" + task.getDescription() + "', "
                    + "'" + task.getComment() + "', "
                    + "'" + task.getStatus() + "'"
                    + ");",
                    Statement.RETURN_GENERATED_KEYS//чтобы возвоащался ID новой записи
            );
            //вернуть ID новой записи
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                resultId = result.getInt(1);

            } else {
                resultId = -1;
            }
            return resultId;

        } catch (SQLException ex) {
            Logger.getLogger(TasksTable.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public Task updateTask(Task task) {

        try {

            Statement stmt = DBManager.getConnection().createStatement();

            boolean success = stmt.execute("UPDATE " + tableName + " SET " + "name='" + task.getName() + "', "
                    + "description='" + task.getDescription() + "', "
                    + "comment='" + task.getComment() + "', "
                    + "status='" + task.getStatus() + "' "
                    + "WHERE id=" + task.getId() + ";"
            );

            if (success) {
                return task;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(TasksTable.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public Task setResponsible(Task task,Person person ){
       
        try {
            String responsible="";
            if(person!=null){
                responsible=person.getId()+"";
            }
            else{
                responsible="null";
            }
            
            Statement stmt = DBManager.getConnection().createStatement();
            //UPDATE `tasktracker`.`task` SET `person_id`='91' WHERE  `id`=321;
            boolean success = stmt.execute("UPDATE " + tableName 
                    + " SET person_id=" + responsible 
                    + " WHERE id=" + task.getId() + ";"
            );

            if (success) {
                return task;
            }
            return null; 
        } catch (SQLException ex) {
            Logger.getLogger(TasksTable.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
}
