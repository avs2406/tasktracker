/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbtest;

/**
 *
 * @author Admin
 */
public class Task {
  private int id;
  private String name="новая задача";
  private String description="";
  private String comment="";
  private String status="";
  private int person_id;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPerson_id(int person_id) {
        this.person_id = person_id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getComment() {
        return comment;
    }

    public String getStatus() {
        return status;
    }

    public int getPerson_id() {
        return person_id;
    }

    @Override
    public String toString() {
        return this.getName();
    }
    
    
    
}
