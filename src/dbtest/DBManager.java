/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbtest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class DBManager {
/**
 * Закроем конструктор так как singletone
 */
    private DBManager() {
    }

    private static String port = "3306";
    private static String host = "109.206.169.221";
    private static String login = "root";
    private static String password = "827KAjh35ls";
    private static String database = "tasktracker";

    private static Connection dbConnection = null;

    public static Connection getConnection() {
        DBManager.init();
        return DBManager.dbConnection;
    }

    protected static boolean init() {

        try {
            if (DBManager.dbConnection != null && !DBManager.dbConnection.isClosed()) {
                return true;
            }

            Class.forName("com.mysql.cj.jdbc.Driver");
            DBManager.dbConnection = DriverManager.getConnection(
                    "jdbc:mysql://" + DBManager.host + ":" + DBManager.port + "/" + DBManager.database + "?useTimeone=true&serverTimezone=GMT&useUniode=yes&characterEncoding=UTF-8",
                    DBManager.login,
                    DBManager.password
            );
            return true;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

}
