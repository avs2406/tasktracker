/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbtest;

import com.mysql.cj.jdbc.result.ResultSetFactory;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class PersonsTable {

    private String tableName = "person";

    //фабричный метод
    private Person producePersonByRs(ResultSet rs) throws SQLException {
        Person resultPerson = new Person();
        resultPerson.setId(rs.getInt("id"));
        resultPerson.setName(rs.getString("name"));
        resultPerson.setLastName(rs.getString("last_name"));
        resultPerson.setEmail(rs.getString("email"));
        resultPerson.setPost(rs.getString("post"));

        return resultPerson;
    }

    Person getById(int id) {
        try {
            Statement stmt = DBManager.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("Select*from " + this.tableName + " where id=" + id + ";");
            if (rs.next()) {
                return this.producePersonByRs(rs);
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonsTable.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ArrayList<Person> getList(String orderBy, String orderDir, ArrayList<Integer> ids, int limit, int page) {
        try {
            Statement stmt = DBManager.getConnection().createStatement();

            String where = "";

            if (ids.size() > 0) {
                where = "Where " + this.tableName + ".id IN (";
            }

            for (int id : ids) {
                where += id + ",";

                where.substring(0, where.length() - 1);
                where += ") ";
            }

            ResultSet rs = stmt.executeQuery("SELECT * FROM "
                    + this.tableName + " \n"
                    + where
                    + "ORDER BY " + this.tableName + "." + orderBy + " " + orderDir
                    + " LIMIT " + limit + " OFFSET " + limit * (page - 1) + " \n"
                    + ";");

            //Создаем массив для результата
            ArrayList<Person> persons = new ArrayList<Person>();

            while (rs.next()) {
                persons.add(this.producePersonByRs(rs));

            }

            return persons;

        } catch (SQLException ex) {
            Logger.getLogger(PersonsTable.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public ArrayList<Person> getList() {
        return this.getList("last_name", "ASC", new ArrayList<Integer>(), 1000, 1);
    }

    boolean deleteById(int id) {
        try {
            Statement stmt = DBManager.getConnection().createStatement();

            stmt.execute("DELETE FROM " + this.tableName + " WHERE id=" + id + ";");

        } catch (SQLException ex) {
            Logger.getLogger(PersonsTable.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public int insertPerson(Person person) {

        try {
            Statement stmt = DBManager.getConnection().createStatement();

            int resultId = stmt.executeUpdate("INSERT INTO " + this.tableName + " (`name`, `last_name`, `email`, `post`)VALUES ("
                    + "'" + person.getName() + "', "
                    + "'" + person.getLastName() + "', "
                    + "'" + person.getEmail() + "', "
                    + "'" + person.getPost() + "'"
                    + ");",
                    Statement.RETURN_GENERATED_KEYS//чтобы возвоащался ID новой записи
            );
            //вернуть ID новой записи
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                resultId = result.getInt(1);

            } else {
                resultId = -1;
            }
            return resultId;

        } catch (SQLException ex) {
            Logger.getLogger(PersonsTable.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public Person updatePerson(Person person) {

        try {

            Statement stmt = DBManager.getConnection().createStatement();

            boolean success = stmt.execute("UPDATE " + this.tableName + " SET " + "name='" + person.getName() + "', "
                    + "last_name='" + person.getLastName() + "', "
                    + "email='" + person.getEmail() + "', "
                    + "post='" + person.getPost() + "' "
                    + "WHERE id=" + person.getId() + ";"
            );

            if (success) {
                return person;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(PersonsTable.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
